<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\SignInPage;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class SignInTest extends DuskTestCase
{
    use DatabaseMigrations;
    
    /** @test */
    public function a_user_can_sign_in()
    {
        $user = factory(User::class)->create([
            'email' => 'iman@gmail.com',
            'password' => bcrypt('secret'),
            'name' => 'Iman Syaefulloh'
        ]);

        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit(new SignInPage)
                ->signIn($user->email, 'secret')
                ->assertPathIs('/home')
                ->assertSeeIn('.navbar', $user->name);
        });
    }
}
