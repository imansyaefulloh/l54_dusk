<?php

namespace Tests\Browser;

use App\Note;
use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\NotesPage;
use Tests\Browser\Pages\SignUpPage;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class NotesTest extends DuskTestCase
{
    use DatabaseMigrations;
    
    /** @test */
    public function a_user_should_see_no_notes_when_starting_their_account()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit(new SignUpPage)
                ->signUp('Iman Syaefulloh', 'iman@gmail.com', 'secret', 'secret')
                ->visit('/home')
                ->assertSee('No notes yet')
                ->assertSee('Untitled')
                ->assertValue('@title', '')
                ->assertValue('@body', '');
        });
    }

    /** @test */
    function a_user_can_save_a_new_note()
    {
        $user = factory(User::class)->create();

        $this->browse(function(Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit(new NotesPage)
                ->typeNote('One', 'One Body')
                ->saveNote()
                ->pause(1500)
                ->assertSeeIn('.alert', 'Your new note has been saved.')
                ->assertSeeIn('.notes', 'One')
                ->assertInputValue('@title', 'One')
                ->assertInputValue('@body', 'One Body');
        });
    }

    /** @test */
    function a_user_can_see_the_word_count_of_their_note()
    {
        $user = factory(User::class)->create();

        $this->browse(function(Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit(new NotesPage)
                ->typeNote('Two', 'There are five words here')
                ->assertSee('Word count: 5');
        });
    }

    /** @test */
    function a_user_can_start_a_fresh_note()
    {
        $user = factory(User::class)->create();

        $this->browse(function(Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit(new NotesPage)
                ->typeNote('Two', 'There are five words here')
                ->saveNote()
                ->pause(1500)
                ->clickLink('Create new note')
                ->pause(1000)
                ->assertSeeIn('.alert', 'A fresh note has been created.')
                ->assertInputValue('@title', '')
                ->assertInputValue('@body', '');
        });
    }

    /** @test */
    function a_users_current_note_is_saved_when_starting_a_new_note()
    {
        $user = factory(User::class)->create();

        $this->browse(function(Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit(new NotesPage)
                ->typeNote('First note', 'First note body')
                ->saveNote()
                ->pause(1500)
                ->type('@title', 'First note updated')
                ->type('@body', 'First note body updated')
                ->clickLink('Create new note')
                ->pause(1000)
                ->assertSeeIn('.notes', 'First note updated')
                ->clickLink('First note updated')
                ->pause(1000)
                ->assertInputValue('@title', 'First note updated')
                ->assertInputValue('@body', 'First note body updated');
        });
    }

    /** @test */
    function a_users_cant_save_note_with_no_title()
    {
        $user = factory(User::class)->create();

        $this->browse(function(Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit(new NotesPage)
                ->saveNote()
                ->pause(1000)
                ->assertMissing('.alert')
                ->assertSeeIn('.notes', 'No notes yet')
                ->assertDontSeeIn('.notes', 'You have one note')
                ->assertMissing('.notes ul li:nth-child(2)');
        });
    }

    /** @test */
    function a_user_can_open_a_previous_note()
    {
        $user = factory(User::class)->create();

        $note = factory(Note::class)->create([
            'user_id' => $user->id
        ]);

        $this->browse(function(Browser $browser) use ($user, $note) {
            $browser->loginAs($user)
                ->visit(new NotesPage)
                ->pause(1000)
                ->clickLink($note->title)
                ->pause(1000)
                ->assertInputValue('@title', $note->title)
                ->assertInputValue('@body', $note->body);
        });
    }

    /** @test */
    function a_user_can_delete_notes()
    {
        $user = factory(User::class)->create();

        $notes = factory(Note::class, 2)->create([
            'user_id' => $user->id
        ]);

        $this->browse(function(Browser $browser) use ($user, $notes) {
            $browser->loginAs($user)
                ->visit(new NotesPage)
                ->pause(1000);

            foreach ($notes as $note) {
                $browser->click('.notes .list-group-item:nth-child(2) a:nth-child(2)')
                    ->pause(1500)
                    ->assertSeeIn('.alert', 'Your note has been deleted.')
                    ->assertDontSeeIn('.notes', $note->title);
            }

            $browser->pause(1000)
                ->assertSeeIn('.notes', 'No notes yet');
        });
    }

    /** @test */
    function a_user_note_is_cleared_when_deleted_if_currently_being_viewed()
    {
        $user = factory(User::class)->create();

        $note = factory(Note::class)->create([
            'user_id' => $user->id
        ]);

        $this->browse(function(Browser $browser) use ($user, $note) {
            $browser->loginAs($user)
                ->visit(new NotesPage)
                ->pause(1000)
                ->clickLink($note->title)
                ->pause(1000)
                ->click('.notes .list-group-item:nth-child(2) a:nth-child(2)') // click delete
                ->pause(1000)
                ->assertInputValue('@title', '')
                ->assertInputValue('@body', '');
        });
    }

    /** @test */
    function a_user_note_are_ordered_by_last_updated_in_descending_order()
    {
        $user = factory(User::class)->create();

        $notes = factory(Note::class, 3)->create([
            'user_id' => $user->id
        ]);

        $this->browse(function(Browser $browser) use ($user, $notes) {
            $browser->loginAs($user)
                ->visit(new NotesPage)
                ->pause(1000)
                ->screenshot('before_sorting');

            foreach ($notes as $note) {
                $browser->clickLink($note->title)
                    ->pause(1000)
                    ->typeNote($newTitle = $note->title . ' updated', 'Woo')
                    ->saveNote()
                    ->pause(1500)
                    ->assertSeeIn('.notes .list-group-item:nth-child(2)', $newTitle);   
            }

            $browser->screenshot('after_sorting');
        });
    }
}
