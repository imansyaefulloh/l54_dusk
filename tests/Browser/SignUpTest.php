<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\SignInPage;
use Tests\Browser\Pages\SignUpPage;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class SignUpTest extends DuskTestCase
{
    use DatabaseMigrations;
    
    /** @test */
    public function a_user_can_sign_up()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit(new SignUpPage)
                ->signUp('Iman Syaefulloh', 'iman@gmail.com', 'secret', 'secret')
                ->assertPathIs('/home')
                ->assertSeeIn('.navbar', 'Iman Syaefulloh');
        });
    }
}
